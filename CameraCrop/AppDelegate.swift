//
//  AppDelegate.swift
//  CameraCrop
//
//  Created by Valentino Urbano on 14/07/17.
//  Copyright © 2017 valentinourbano. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    return true
  }

}

