//
//  TargaImageController.swift
//  Volkswagen
//
//  Created by Valentino Urbano on 14/07/17.
//  Copyright © 2017 valentinourbano. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit
import AVKit
import AVFoundation

class CameraCropController: UIViewController {

  private var _session: AVCaptureSession? = nil
  private var _stillImageOutput: AVCaptureStillImageOutput? = nil
  private var _isSetup = false
  private var _captureVideoPreviewLayer: AVCaptureVideoPreviewLayer? = nil

  var capturedImage: UIImage? = nil

  @IBOutlet weak var liveCapturePlaceholderView: UIView!

  @IBOutlet weak var bottomControlsView: UIView!

  @IBOutlet weak var captureImageRect: UIView!

  @IBOutlet weak var shutterButtonBackground: UIView!

  @IBOutlet weak var shutterButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()

    shutterButtonBackground.layer.cornerRadius = shutterButtonBackground.frame.width/2
    shutterButtonBackground.backgroundColor = .black
    bottomControlsView.backgroundColor = .black

    captureImageRect.backgroundColor = UIColor.clear
    captureImageRect.layer.borderColor = UIColor.red.cgColor
    captureImageRect.layer.borderWidth = 1
    captureImageRect.layer.zPosition = 10//front

    askForAuth()
  }


  func askForAuth() {

    let authorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
    switch authorizationStatus {
    case .notDetermined:
      // permission dialog not yet presented, request authorization
      AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo,
                                                completionHandler: { [weak self] (granted:Bool) -> Void in
                                                  if granted {
                                                    // go ahead
                                                    self?.initialSetup()
                                                  }
                                                  else {
                                                    // user denied, nothing much to do
                                                    self?.handleFailureAuth()
                                                    return
                                                  }
      })
    case .authorized:
    // go ahead
      initialSetup()
      return
    case .denied, .restricted:
      // the user explicitly denied camera usage or is not allowed to access the camera devices
      handleFailureAuth()
      return
    }

  }

  func initialSetup() {
    _session = AVCaptureSession()
    guard let session = _session else {
      print("session nil")
      handleFailure()
      return
    }
    session.sessionPreset = AVCaptureSessionPresetPhoto

    var backCameraDevice: AVCaptureDevice? = nil
    var frontCameraDevice: AVCaptureDevice? = nil
    let availableCameraDevices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo)
    guard let cameras = availableCameraDevices as? [AVCaptureDevice] else {
      print("no camera")
      handleFailureAuth()
      return
    }
    for device in cameras {
      if device.position == .back {
        backCameraDevice = device
      } else if device.position == .front {
        frontCameraDevice = device
      }
    }

    let cameraToUse: AVCaptureDevice? = backCameraDevice ?? frontCameraDevice ?? nil
    guard let cameraId = cameraToUse?.uniqueID else {
      print("camera has no id")
      handleFailureAuth()
      return
    }

    let camera = AVCaptureDevice(uniqueID: cameraId)
    if (camera == nil) {
      print("no camera with id")
      handleFailureAuth()
      return
    }
    guard let captureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session:session) else {
      print("av capture session nil")
      handleFailure()
      return
    }
    captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill

    captureVideoPreviewLayer.frame = self.liveCapturePlaceholderView.bounds
    self.liveCapturePlaceholderView.layer.addSublayer(captureVideoPreviewLayer)
    _captureVideoPreviewLayer = captureVideoPreviewLayer

    guard let input = try? AVCaptureDeviceInput(device: camera) else {
      print("AVCaptureDeviceInput failure")
      handleFailure()
      return
    }

    session.addInput(input)

    let options = [ AVVideoCodecKey : AVVideoCodecJPEG ]
    _stillImageOutput = AVCaptureStillImageOutput()
    if _stillImageOutput == nil {
      print("_stillImageOutput failure")
      handleFailure()
      return
    }
    _stillImageOutput!.outputSettings = options

    session.addOutput(_stillImageOutput!)

    session.startRunning()
    _isSetup = true

  }

  func handleFailure() {
    print("failure")//TODO: Handle failure
    return
  }

  func handleFailureAuth() {
    print("auth failure")//TODO: Handler failure
    return
  }

  @IBAction func shutterButtonTapped(_ sender: UIButton) {
    if _isSetup {
      captureImage()
    } else {
      print("not setup yet")
    }
  }

  func captureImage() {
    guard let videoConnection = _stillImageOutput?.connection(withMediaType: AVMediaTypeVideo) else {
      print("no video connection")
      handleFailure()
      return
    }

    _stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: { [weak self] (imageSampleBuffer, error) in

      if (error != nil) {
      print("Error capturing image from camera: \(error.debugDescription)")
        self?.handleFailure()
      }else{
        guard let imageData: Data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageSampleBuffer) else {
          print("no image data")
          self?.handleFailure()
          return
        }
        guard let image = UIImage(data:imageData) else {
          print("image nil")
          self?.handleFailure()
          return
        }

        guard let croppedImage = self?.cropToPreviewLayer(originalImage: image) else {
          print("cropped image nil")
          self?.handleFailure()
          return
        }

      self?.capturedImage = croppedImage
        if self?._session == nil {
          print("no session")
          self?.handleFailure()
          return
        }
      self?._session!.stopRunning()
        //TODO: Found image, pass "croppedImage" as result
      }
    })


  }

  private func cropToPreviewLayer(originalImage: UIImage) -> UIImage? {
    guard let _captureVideoPreviewLayer = self._captureVideoPreviewLayer else {
      print("no preview layer")
      self.handleFailure()
      return nil
    }

    let outputRect = _captureVideoPreviewLayer.metadataOutputRectOfInterest(for: captureImageRect.frame)
    var cgImage = originalImage.cgImage!
    let width = CGFloat(cgImage.width)
    let height = CGFloat(cgImage.height)
    let cropRect = CGRect(x: outputRect.origin.x * width, y: outputRect.origin.y * height, width: outputRect.size.width * width, height: outputRect.size.height * height)

    cgImage = cgImage.cropping(to: cropRect)!
    let croppedUIImage = UIImage(cgImage: cgImage, scale: 1.0, orientation: originalImage.imageOrientation)

    return croppedUIImage
  }

}
